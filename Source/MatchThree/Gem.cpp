// Fill out your copyright notice in the Description page of Project Settings.


#include "Gem.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AGem::AGem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	displayMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("The shape of the gem"));

	//displayMesh->SetupAttachment(RootComponent);
	RootComponent = displayMesh;

	//if (displayMesh != NULL) {
	//	UE_LOG(LogTemp, Warning, TEXT("The Mesh is not null"));
	//}
	//else {
	//	UE_LOG(LogTemp, Warning, TEXT("The Mesh is null"));
	//}
}

// Called when the game starts or when spawned
void AGem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGem::Tick(float DeltaTime)
{
	

	if (isMoving) {
		FVector deltaLoc = moveTo - GetActorLocation();
		float dist = deltaLoc.Size();

		float maxMoveDist = moveSpeed * DeltaTime;
		//UE_LOG(LogTemp, Warning, TEXT("The maxMoveDist is %f"), maxMoveDist);
		//UE_LOG(LogTemp, Warning, TEXT("The moveSpeed is %f"), moveSpeed);
		//UE_LOG(LogTemp, Warning, TEXT("The deltaTime is %d"), DeltaTime);
		//UE_LOG(LogTemp, Warning, TEXT("The distance is %f"), dist);

		//UE_LOG(LogTemp, Warning, TEXT("Moving to           %f, %f, %f"), moveTo.X, moveTo.Y, moveTo.Z);
		//UE_LOG(LogTemp, Warning, TEXT("Current Location is %d, %d, %d"), GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z);

		if (dist < maxMoveDist) {
			SetActorLocation(moveTo);
			isMoving = false;
			//UE_LOG(LogTemp, Warning, TEXT("Movement complete"));
		}
		else {
			FVector newLoc;
			FVector direction = deltaLoc / dist;
			//UE_LOG(LogTemp, Warning, TEXT("Direction           %f, %f, %f"), direction.X, direction.Y, direction.Z);
			newLoc = GetActorLocation() + direction * maxMoveDist;
			//UE_LOG(LogTemp, Warning, TEXT("New Loc             %f, %f, %f"), newLoc.X, newLoc.Y, newLoc.Z);
			SetActorLocation(newLoc);
		}

	}
	Super::Tick(DeltaTime);
}


void AGem::setGemColour(UMaterialInterface* newColour) {
	gemColour = newColour;
}
