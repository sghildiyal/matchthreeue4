// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchThreePlayerPawn.h"

// Sets default values
AMatchThreePlayerPawn::AMatchThreePlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMatchThreePlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMatchThreePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMatchThreePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

