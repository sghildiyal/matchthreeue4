// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MatchThreeGameState.generated.h"

/**
 * 
 */
UCLASS()
class MATCHTHREE_API AMatchThreeGameState : public AGameStateBase
{
	GENERATED_BODY()
};
