// Fill out your copyright notice in the Description page of Project Settings.


#include "Grid.h"
#include "Engine/World.h"
//#include "Rand"

// Sets default values
AGrid::AGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	gemsAreMoving = false;
}

// Called when the game starts or when spawned
void AGrid::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGrid::generateGrid()
{

	UE_LOG(LogTemp, Warning, TEXT("X Gap %d"), xGap);
	UE_LOG(LogTemp, Warning, TEXT("Y Gap %d"), yGap);

	if (cellClass == NULL) {
		return;
	}
	UWorld* world = GetWorld();
	if (world == NULL) {
		return;
	}

	FVector startPos = GetActorLocation();
	UE_LOG(LogTemp, Warning, TEXT("StartPos %f %f %f"), startPos.X, startPos.Y, startPos.Z);
	FActorSpawnParameters spawnParams;
	//spawnParams.Owner = this;
	FRotator rotator;
	FVector newLoc;
	FVector deltaLoc;

	for (int row = 0; row < rows; row++) {
		//FCellRow row;
		for (int column = 0; column < columns; column++) {
			UE_LOG(LogTemp, Warning, TEXT("I, J:  %d, %d"), row, column);
			UE_LOG(LogTemp, Warning, TEXT("newX, newY:  %d, %d"), xGap * column, yGap * row);
			deltaLoc.Set(xGap * column, yGap * row, 0);
			
			//deltaLoc.X = xGap * i;
			//deltaLoc.Y = yGap * j;
			UE_LOG(LogTemp, Warning, TEXT("DeltaLoc %f %f %f"), deltaLoc.X, deltaLoc.Y, deltaLoc.Z);
			newLoc = startPos + deltaLoc;
			UE_LOG(LogTemp, Warning, TEXT("A new Cell should be spawned at %f %f %f"), newLoc.X, newLoc.Y, newLoc.Z);
			ACell* cell = world->SpawnActor<ACell>(cellClass, newLoc, rotator, spawnParams);
			UE_LOG(LogTemp, Warning, TEXT("The new Cell was spawned at %f %f %f"), cell->GetActorLocation().X, cell->GetActorLocation().Y, cell->GetActorLocation().Z);
			cells.Add(cell);

			if (cell->neighbours.Num() != 4) {
				cell->neighbours.Empty();
				for (int i = 0; i < 4; i++) {
					cell->neighbours.Add(nullptr);
				}
			}
		}
		//cellRows.Add(row);
	}

}

void AGrid::connectCells() {
	// assuming a square grid

	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {
			ACell* cell = getCell(row, column);

			

			// down
			if (row > 0) {
				//cell->down = getCell(row - 1, column);
				cell->neighbours[2] = getCell(row - 1, column);
			}
			else {
				//cell->down = nullptr;
				cell->neighbours[2] = nullptr;
			}
			 
			//up
			if (row < rows - 1) {
				//cell->up = getCell(row + 1, column);
				cell->neighbours[0] = getCell(row + 1, column);
			}
			else {
				//cell->up = NULL;
				cell->neighbours[0] = nullptr;
			}

			//left
			if (column > 0) {
				//cell->left = getCell(row, column - 1);
				cell->neighbours[3] = getCell(row, column - 1);
			}
			else {
				//cell->left = NULL;
				cell->neighbours[3] = nullptr;
			}

			//right
			if (column < columns - 1) {
				//cell->right = getCell(row, column + 1);
				cell->neighbours[1] = getCell(row, column + 1);
			}
			else {
				//cell->right = NULL;
				cell->neighbours[1] = nullptr;
			}
		}
	}
}

void AGrid::createInitialGems() {

	ACell* cell;

	if (gemClass == NULL) {
		return;
	}
	UWorld* world = GetWorld();
	if (world == NULL) {
		return;
	}

	
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	FRotator rotator;

	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {

			//wrongMaterials.Empty();
			cell = getCell(row, column);

			AGem* gem = world->SpawnActor<AGem>(gemClass, cell->GetActorLocation() + cell->gemOffset, rotator, spawnParams);
			cell->gem = gem;
		}
	}

	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {
			cell = getCell(row, column);
			cell->setNonMatchingGemColour(validMaterials);
		}
	}
}

// Called every frame
void AGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (gemsAreMoving) {
		checkForMovingGems();
	} 
}


void AGrid::addGems() {

	int* missingGems = new int[columns];

	for (int column = 0; column < columns; column++) {
		missingGems[column] = 0;
	}

	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {
			if (getCell(row, column)->gem == NULL) {
				missingGems[column] += 1;
			}
			
		}
	}
}


ACell* AGrid::getCell(int row, int column) {
	return cells[row * columns + column];
}

void AGrid::doGemSwap(ACell* cell1, ACell* cell2) {

	currentPhase = GamePhase::GemSwapping;

	gemsAreMoving = true;

	swapCell1 = cell1;
	swapCell2 = cell2;

	AGem* gem1 = cell1->gem;
	AGem* gem2 = cell2->gem;

	cell1->gem = gem2;
	cell2->gem = gem1;

	FVector moveToLoc;
	
	cell1->gem->isMoving = true;
	moveToLoc = cell1->GetActorLocation() + cell1->gemOffset;
	cell1->gem->moveTo = moveToLoc;
	UE_LOG(LogTemp, Warning, TEXT("Cell1 loc           %f, %f, %f"), cell1->GetActorLocation().X, cell1->GetActorLocation().Y, cell1->GetActorLocation().Z);

	cell2->gem->isMoving = true;
	moveToLoc = cell2->GetActorLocation() + cell2->gemOffset;
	cell2->gem->moveTo = moveToLoc;
	UE_LOG(LogTemp, Warning, TEXT("Cell2 loc           %f, %f, %f"), cell2->GetActorLocation().X, cell2->GetActorLocation().Y, cell2->GetActorLocation().Z);


	return;
}

void AGrid::checkForMovingGems() {

	int cells_count = cells.Num();

	bool foundMovingGem = false;

	for (int i = 0; i < cells_count; i++) {
		if (cells[i]->gem->isMoving) {
			foundMovingGem = true;
			break;
		}
	}
	

	if (!foundMovingGem) {
		//gemsAreMoving = false;
		//gameState->gemMovementComplete();
		UE_LOG(LogTemp, Warning, TEXT("No moving gem was found"));
		//UE_LOG(LogTemp, Warning, TEXT("The current phase is %d"), currentPhase);
		if (currentPhase == GamePhase::GemSwapping) {
			//currentPhase = GamePhase::CheckValidMove;
			UE_LOG(LogTemp, Warning, TEXT("The current phase is GemSwapping"));
			TArray<ACell*> matchedCells = getCellsWithMatches();
			if (matchedCells.Num() > 0) { // technically this can never be 1 or 2
				//currentPhase = GamePhase::DestroyGems;
				destroyGems(matchedCells);
				addReassignGems();
				swapCell1 = nullptr;
				swapCell2 = nullptr;
				gemsAreMoving = true;
				currentPhase = GamePhase::DropGems;
			}
			else {
				doSwapBack();
				currentPhase = GamePhase::GemSwapBack;
				gemsAreMoving = true;
			}
		}
		else if (currentPhase == GamePhase::GemSwapBack) {
			UE_LOG(LogTemp, Warning, TEXT("The current phase is GemSwapBack"));
			currentPhase = GamePhase::WaitingUserInput;
			swapCell1 = nullptr;
			swapCell2 = nullptr;
			gemsAreMoving = false;
		}
		else if (currentPhase == GamePhase::DropGems) {
			TArray<ACell*> matchedCells = getCellsWithMatches();
			//currentPhase = GamePhase::CheckMatches;
			if (matchedCells.Num() > 0) { // technically this can never be 1 or 2
				//currentPhase = GamePhase::DestroyGems;
				destroyGems(matchedCells);
				addReassignGems();
				swapCell1 = nullptr;
				swapCell2 = nullptr;
				gemsAreMoving = true;
				currentPhase = GamePhase::DropGems;
			}
			//gemsAreMoving = true;
			else {

				while (!isValidMovePresent()) {
					UE_LOG(LogTemp, Warning, TEXT("No Valid Move was possible, destroying and creating new gems"));
					destroyGems(cells);
					createInitialGems();
				}
				
				currentPhase = GamePhase::WaitingUserInput;
				//swapCell1 = nullptr;
				//swapCell2 = nullptr;
				gemsAreMoving = false;
				resetMatchSizes();
				
			}
		}
	}
}

TArray<ACell*> AGrid::getCellsWithMatches()
{
	TArray<ACell*> matchedCells;
	ACell* cell;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			cell = getCell(i, j);

			if (cell->isMatching(cell->gem->gemColour, -1) >= 3) {
				matchedCells.Add(cell);
			}
		}
	}

	return matchedCells;
}

void AGrid::destroyGems(TArray<ACell*> matchedCells)
{	
	AGem* gem;
	for (int i = 0; i < matchedCells.Num(); i++) {
		gem = matchedCells[i]->gem;
		if (gem != nullptr) {
			gem->Destroy();
			matchedCells[i]->gem = nullptr;
			if (matchedCells[i]->partOfMatchSize > 0) {
				score += matchedCells[i]->partOfMatchSize - 2;
			}
			
		}
	}
}

void AGrid::addReassignGems()
{
	ACell* cell;
	bool foundGem;
	int gemsAddedInColumn;
	for (int column = 0; column < columns; column++) {
		gemsAddedInColumn = 0;
		for (int row = 0; row < rows; row++) {
			cell = getCell(row, column);
			if (cell->gem == nullptr) {
				foundGem = cell->acquireFirstGemInDirection(0);
				if (!foundGem) {
					gemsAddedInColumn += 1;
					cell->spawnAddNewgem(gemsAddedInColumn * yGap, validMaterials);
				}
			}
		}
	}
}

void AGrid::doSwapBack()
{
	AGem* gem1 = swapCell1->gem;
	AGem* gem2 = swapCell2->gem;

	swapCell1->gem = gem2;
	swapCell2->gem = gem1;

	FVector moveToLoc;

	swapCell1->gem->isMoving = true;
	moveToLoc = swapCell1->GetActorLocation() + swapCell1->gemOffset;
	swapCell1->gem->moveTo = moveToLoc;
	UE_LOG(LogTemp, Warning, TEXT("Cell1 loc           %f, %f, %f"), swapCell1->GetActorLocation().X, swapCell1->GetActorLocation().Y, swapCell1->GetActorLocation().Z);

	swapCell2->gem->isMoving = true;
	moveToLoc = swapCell2->GetActorLocation() + swapCell2->gemOffset;
	swapCell2->gem->moveTo = moveToLoc;
	UE_LOG(LogTemp, Warning, TEXT("Cell2 loc           %f, %f, %f"), swapCell2->GetActorLocation().X, swapCell2->GetActorLocation().Y, swapCell2->GetActorLocation().Z);
}

bool AGrid::isValidMovePresent()
{
	ACell* cell;

	bool possibleMove = false;

	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {
			UE_LOG(LogTemp, Warning, TEXT("Checking possible move for cell %d, %d"), row, column);
			cell = getCell(row, column);
			if (cell->isMatchingMovePossible()) {
				possibleMove = true;
				UE_LOG(LogTemp, Warning, TEXT("Move possible for cell %d, %d"), row, column);
			}
		}
	}

	return possibleMove;
}

void AGrid::resetMatchSizes()
{
	ACell* cell;
	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {
			cell = getCell(row, column);
			cell->partOfMatchSize = 0;
		}
	}
}

void AGrid::resetGame()
{
	destroyGems(cells);
	createInitialGems();

	score = 0;
	totalTime = 0;
}