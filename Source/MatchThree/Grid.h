// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cell.h"
#include "Grid.generated.h"

UENUM()
enum GamePhase {
	WaitingUserInput		UMETA(DisplayName = "WaitingUserInput"),
	GemSwapping				UMETA(DisplayName = "GemSwapping"),
	CheckValidMove			UMETA(DisplayName = "CheckValidMove"),
	GemSwapBack				UMETA(DisplayName = "GemSwapBack"),
	CheckMatches			UMETA(DisplayName = "CheckMatches"),
	DestroyGems				UMETA(DisplayName = "DestroyGems"),
	CreateNewGems			UMETA(DisplayName = "CreateNewGems"),
	DropGems				UMETA(DisplayName = "DropGems"),
};


UCLASS()
class MATCHTHREE_API AGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrid();

	UPROPERTY(EditAnywhere, Category = "Grid")
	int columns;

	UPROPERTY(EditAnywhere, Category = "Grid")
	int rows;

	UPROPERTY(EditAnywhere, Category = "Grid")
	int xGap;

	UPROPERTY(EditAnywhere, Category = "Grid")
	int yGap;

	UPROPERTY(BlueprintReadWrite, Category = "Grid")
	TArray<ACell*> cells;

	UPROPERTY(EditAnywhere, Category = "Grid")
	TSubclassOf<class ACell> cellClass;

	UPROPERTY(EditAnywhere, Category = "Grid")
	TSubclassOf<class AGem> gemClass;

	UPROPERTY(BlueprintReadWrite, Category = "Grid")
	TArray<UMaterialInterface*> validMaterials;

	UPROPERTY(BlueprintReadWrite, Category = "Play")
	bool gemsAreMoving;

	UPROPERTY(BlueprintReadWrite, Category = "Status")
	TEnumAsByte<GamePhase> currentPhase;

	UPROPERTY(BlueprintReadWrite, Category = "Play")
	ACell* swapCell1;

	UPROPERTY(BlueprintReadWrite, Category = "Play")
	ACell* swapCell2;

	UPROPERTY(BlueprintReadWrite, Category = "Play")
	int score;

	UPROPERTY(BlueprintReadWrite, Category = "Play")
	float totalTime;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category= "Creation")
	void generateGrid();

	UFUNCTION(BlueprintCallable, Category = "Creation")
	void connectCells();

	UFUNCTION(BlueprintCallable, Category = "Creation")
	void createInitialGems();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void addGems();

	UFUNCTION(BlueprintCallable, Category = "Grid")
	ACell* getCell(int row, int column);

	UFUNCTION(BlueprintCallable, Category = "Play")
	void doGemSwap(ACell* cell1, ACell* cell2);

	UFUNCTION(BlueprintCallable, Category = "Play")
	void doSwapBack();

	UFUNCTION(BlueprintCallable, Category = "Play")
	void checkForMovingGems();

	UFUNCTION(BlueprintCallable, Category = "Matching")
	TArray<ACell*> getCellsWithMatches();

	UFUNCTION(BlueprintCallable, Category = "Play")
	void destroyGems(TArray<ACell*> matchedCells);

	UFUNCTION(BlueprintCallable, Category="Play")
	void addReassignGems();

	UFUNCTION(BlueprintCallable, Category = "Play")
	bool isValidMovePresent();

	UFUNCTION(BlueprintCallable, Category = "Matching")
	void resetMatchSizes();

	UFUNCTION(BlueprintCallable, Category = "Matching")
	void resetGame();
};
