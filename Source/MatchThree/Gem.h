// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gem.generated.h"

UCLASS()
class MATCHTHREE_API AGem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGem();

	UPROPERTY(EditAnywhere, Category = "Mesh")
	UStaticMeshComponent* displayMesh;

	UPROPERTY(BlueprintReadWrite, Category = "Material")
	UMaterialInterface* gemColour;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float moveSpeed;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	bool isMoving;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	FVector moveTo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Material")
	void setGemColour(UMaterialInterface* newColour);
};
