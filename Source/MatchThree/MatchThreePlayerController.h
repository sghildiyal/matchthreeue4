// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Cell.h"
#include "Grid.h"
#include "MatchThreePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MATCHTHREE_API AMatchThreePlayerController : public APlayerController
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadWrite, Category = "GameObjects")
	ACell* currentSelectedCell;

	UPROPERTY(BlueprintReadWrite, Category = "GameObjects")
	AGrid* gameGrid;

	UFUNCTION(BlueprintCallable, Category = "Mouse")
	void handleMouseSelect(bool hitOccured, ACell* hitCell);
	
	UFUNCTION(BlueprintCallable, Category = "GameActions")
	void doGemSwap(ACell* cell1, ACell* cell2);
};
