// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gem.h"
#include "Cell.generated.h"


UCLASS()
class MATCHTHREE_API ACell : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACell();

	UPROPERTY(BlueprintReadWrite, Category = "Neighbours")
	TArray<ACell*> neighbours;
	
	UPROPERTY(BlueprintReadWrite, Category = "Gem")
	AGem* gem;

	UPROPERTY(BlueprintReadWrite, Category = "Materials")
	UMaterialInterface* defaultMaterial;

	UPROPERTY(BlueprintReadWrite, Category = "Materials")
	UMaterialInterface* neighbourMaterial;

	UPROPERTY(BlueprintReadWrite, Category = "Materials")
	TArray<UMaterialInterface*> neighbourMaterials; // indices should coincide with the directions. Also present entirely for dev

	UPROPERTY(BlueprintReadWrite, Category = "Materials")
	UMaterialInterface* hoverMaterial;

	UPROPERTY(BlueprintReadWrite, Category = "Materials")
	UMaterialInterface* possibleMatchColour;

	UPROPERTY(BlueprintReadOnly, Category = "Mesh")
	UStaticMeshComponent* displayMesh;

	UPROPERTY(EditAnywhere, Category = "Gem")
	TSubclassOf<class AGem> gemClass;

	UPROPERTY(EditAnywhere, Category = "Gem")
	FVector gemOffset;

	UPROPERTY(BlueprintReadWrite, Category = "Matching")
	bool validMovePossible;

	UPROPERTY(BlueprintReadWrite, Category = "Matching")
	int partOfMatchSize;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void startMouseSelect();

	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void endMouseSelect();

	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void startNeighbourMouseSelect();

	UFUNCTION(BlueprintCallable, Category = "Interaction")
		void startDirectionNeighbourMouseSelect(int directionIndex);

	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void endNeighbourMouseSelect();

	UFUNCTION(BlueprintCallable, Category = "Gem")
	void createInitialGem();

	UFUNCTION(BlueprintCallable, Category = "Gem")
	void setNonMatchingGemColour(TArray<UMaterialInterface*> validMaterials);

	UFUNCTION(BlueprintCallable, Category = "Neighbours")
	bool isNeighbourOf(ACell* otherCell);

	UFUNCTION(BlueprintCallable, Category = "Matching")
	int isMatching(UMaterialInterface* gemColour, int ignoreDirection);

	UFUNCTION(BlueprintCallable, Category = "Gem")
	bool acquireFirstGemInDirection(int directionIndex);

	UFUNCTION(BlueprintCallable, Category = "Gem")
	void spawnAddNewgem(int yOffset, TArray<UMaterialInterface*> validMaterials);

	UFUNCTION(BlueprintCallable, Category = "Matching")
	bool isMatchingMovePossible();

	UFUNCTION(BlueprintCallable, Category = "Matching")
	bool isSwapFromDirectionValid(int dirIndex);

	UFUNCTION(BlueprintCallable, Category = "Matching")
	int getOppositeDirectionIndex(int directionIndex);

	UFUNCTION(BlueprintCallable, Category = "Matching")
	int getTotalMatchesInDirection(int directionIndex, UMaterialInterface* matchColour);

	UFUNCTION(BlueprintCallable, Category = "Matching")
	void updatePartOfMatchSize(int dirIndex, int size);
};
