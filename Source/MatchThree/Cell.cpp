// Fill out your copyright notice in the Description page of Project Settings.


#include "Cell.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ACell::ACell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	displayMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("The shape of the cell"));

	//mesh->SetupAttachment(RootComponent);
	RootComponent = displayMesh;

	//if (displayMesh != NULL) {
	//	UE_LOG(LogTemp, Warning, TEXT("The Mesh is not null"));
	//}
	//else {
	//	UE_LOG(LogTemp, Warning, TEXT("The Mesh is null"));
	//}

	/*neighbours = { nullptr, nullptr, nullptr, nullptr };*/
	
}

// Called when the game starts or when spawned
void ACell::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACell::startMouseSelect() {

	displayMesh->SetMaterial(0, hoverMaterial);

	for (int dirIndex = 0; dirIndex < neighbours.Num(); dirIndex++) {
		if (neighbours[dirIndex] != nullptr) {
			neighbours[dirIndex]->startDirectionNeighbourMouseSelect(dirIndex);
		}
		
	}
}

void ACell::endMouseSelect() {

	displayMesh->SetMaterial(0, defaultMaterial);

	for (int dirIndex = 0; dirIndex < neighbours.Num(); dirIndex++) {
		if (neighbours[dirIndex] != nullptr) {
			neighbours[dirIndex]->endNeighbourMouseSelect();
		}
	}
}

void ACell::startDirectionNeighbourMouseSelect(int directionIndex) {
	displayMesh->SetMaterial(0, neighbourMaterials[directionIndex]);
}

void ACell::startNeighbourMouseSelect() {
	displayMesh->SetMaterial(0, neighbourMaterial);
}

void ACell::endNeighbourMouseSelect() {
	displayMesh->SetMaterial(0, defaultMaterial);
}

void ACell::createInitialGem() {

	if (gemClass == NULL) {
		return;
	}
	UWorld* world = GetWorld();
	if (world == NULL) {
		return;
	}

	FVector currentPos = GetActorLocation();
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	FRotator rotator;
	gem = world->SpawnActor<AGem>(gemClass, currentPos + gemOffset, rotator, spawnParams);
}

void ACell::setNonMatchingGemColour(TArray<UMaterialInterface*> validMaterials) {
	UMaterialInterface* selectedMaterial;

	TArray<int> badMaterialIndices;
	TArray<int> goodMaterialIndices;

	//UE_LOG(LogTemp, Warning, TEXT("valid material countis %d"), validMaterials.Num());
	bool isBadMaterial;
	for (int materialIndex = 0; materialIndex < validMaterials.Num(); materialIndex++) {
		isBadMaterial = false;
		for (int dirIndex = 0; dirIndex < neighbours.Num(); dirIndex++) {
			if (neighbours[dirIndex] != nullptr) {
				if (neighbours[dirIndex]->neighbours[dirIndex] != nullptr) {
					UMaterialInterface* vm = validMaterials[materialIndex];
					if (neighbours[dirIndex]->gem->gemColour == validMaterials[materialIndex]) {
						if (neighbours[dirIndex]->neighbours[dirIndex]->gem->gemColour == validMaterials[materialIndex]) {
							badMaterialIndices.Add(materialIndex);
							isBadMaterial = true;
							continue;
						}
					}
				}
			}
		}
		
		if (!isBadMaterial) {
			goodMaterialIndices.Add(materialIndex);
		}
	}

	//UE_LOG(LogTemp, Warning, TEXT("good material countis %d"), goodMaterialIndices.Num());

	int randomIndex = FMath::RandRange(0, goodMaterialIndices.Num() - 1);

	//UE_LOG(LogTemp, Warning, TEXT("valid colour index is %d"), goodMaterialIndices[randomIndex]);

	selectedMaterial = validMaterials[goodMaterialIndices[randomIndex]];

	gem->gemColour = selectedMaterial;

	if (gem->displayMesh != NULL) {
		gem->displayMesh->SetMaterial(0, selectedMaterial);
	}
}

bool ACell::isNeighbourOf(ACell* otherCell) {

	if (otherCell == nullptr) {
		return false;
	}

	for (int i = 0; i < neighbours.Num(); i++) {
		if (otherCell == neighbours[i]) {
			return true;
		}
	}

	return false;
}


int ACell::isMatching(UMaterialInterface* gemColour, int ignoreDirection) {

	int matchSize = 0;
	int matchSizes[] = { 0, 0, 0, 0 };
	for (int dirIndex = 0; dirIndex < neighbours.Num(); dirIndex++) {

		if (dirIndex == ignoreDirection) {
			continue;
		}
		matchSizes[dirIndex] = getTotalMatchesInDirection(dirIndex, gemColour);
	}

	//UE_LOG(LogTemp, Warning, TEXT("matches %d, %d, %d, %d"), matchSizes[0], matchSizes[1], matchSizes[2], matchSizes[3]);

	int oppDirIndex;
	for (int dirIndex = 0; dirIndex < neighbours.Num() / 2; dirIndex++) {
		oppDirIndex = getOppositeDirectionIndex(dirIndex);
		if (matchSizes[dirIndex] + matchSizes[oppDirIndex] + 1 >= 3) {
			matchSize += matchSizes[dirIndex] + matchSizes[oppDirIndex];
		}
		else {
			matchSizes[dirIndex] = 0;
			matchSizes[oppDirIndex] = 0;
		}
	}

	if (matchSize >= 2) {

		matchSize += 1;

		if (partOfMatchSize < matchSize) {
			partOfMatchSize = matchSize;
		}

		for (int dirIndex = 0; dirIndex < neighbours.Num(); dirIndex++) {
			if (matchSizes[dirIndex] > 0) {
				if (neighbours[dirIndex] != nullptr) {
					neighbours[dirIndex]->updatePartOfMatchSize(dirIndex, matchSize);
				}
			}
		}

		return matchSize;
	}
	else {
		return 0;
	}
}

// The return values is false if there was no gem above to acquire. In that case we should be spawning new gems
bool ACell::acquireFirstGemInDirection(int directionIndex)
{
	ACell* acquireFrom = neighbours[directionIndex];

	while (acquireFrom != nullptr) {
		if (acquireFrom->gem != nullptr) {
			gem = acquireFrom->gem;
			gem->isMoving = true;
			gem->moveTo = GetActorLocation() + gemOffset;
			acquireFrom->gem = nullptr;
			return true;
		}
		else {
			acquireFrom = acquireFrom->neighbours[directionIndex];
		}
	}

	return false;
}

void ACell::spawnAddNewgem(int yOffset, TArray<UMaterialInterface*> validMaterials) {
	
	UWorld* world = GetWorld();
	if (world == NULL) {
		return;
	}
	
	FVector spawnLocation;

	ACell* locAnchorCell = this;

	while (locAnchorCell->neighbours[0] != nullptr) {
		locAnchorCell = locAnchorCell->neighbours[0];
	}

	spawnLocation = locAnchorCell->GetActorLocation() + gemOffset;

	spawnLocation.Y += yOffset;

	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	FRotator rotator;

	gem = world->SpawnActor<AGem>(gemClass, spawnLocation, rotator, spawnParams);

	gem->moveTo = GetActorLocation() + gemOffset;
	//gem->gemColour = rand()
	gem->isMoving = true;

	int randomIndex = FMath::RandRange(0, validMaterials.Num() - 1);
	gem->gemColour = validMaterials[randomIndex];

	if (gem->displayMesh != NULL) {
		gem->displayMesh->SetMaterial(0, validMaterials[randomIndex]);
	}
	//return false;
}


bool ACell::isMatchingMovePossible()
{
	validMovePossible = false;

	for (int dirIndex = 0; dirIndex < neighbours.Num(); dirIndex++) {
		if (neighbours[dirIndex] != nullptr) {
			//UE_LOG(LogTemp, Warning, TEXT("direction %d  opposite %d"), dirIndex, getOppositeDirectionIndex(dirIndex));
			validMovePossible = neighbours[dirIndex]->isSwapFromDirectionValid(getOppositeDirectionIndex(dirIndex));
			if (validMovePossible) {
				break;
			}
		}
	}

	if (validMovePossible) {
		displayMesh->SetMaterial(0, possibleMatchColour);
	}
	else {
		displayMesh->SetMaterial(0, defaultMaterial);
	}
	
	return validMovePossible;
}


/*
	Check if a match is possible if the gem from the given direction is swapped to this cell

	Basically check if the new gem would match the cells in any of the other directions
*/
bool ACell::isSwapFromDirectionValid(int dirIndex) {

	if (neighbours[dirIndex] == nullptr) {
		return false;
	}

	UMaterialInterface* matchColour = neighbours[dirIndex]->gem->gemColour;

	return (isMatching(matchColour, dirIndex) > 0);

	
}

int ACell::getOppositeDirectionIndex(int directionIndex)
{
	/*
		In case of square grid
			up = 0
			right = 1
			down = 2
			left = 3
	*/
	int neighCount = neighbours.Num();
	if (directionIndex >= neighCount) {
		// We should be throwing an exception here
		return -1;
	}
	else {
		return ((directionIndex + (neighCount / 2)) % neighCount);
	}
	
}

int ACell::getTotalMatchesInDirection(int directionIndex, UMaterialInterface* matchColour)
{
	int sum = 0;

	ACell* neighbour = neighbours[directionIndex];

	if (neighbour != nullptr) {
		if (neighbour->gem->gemColour == matchColour) {
			sum = neighbour->getTotalMatchesInDirection(directionIndex, matchColour) + 1;
		}
	}

	return sum;
}

void ACell::updatePartOfMatchSize(int dirIndex, int size)
{
	if (partOfMatchSize < size) {
		partOfMatchSize = size;
	}

	if (neighbours[dirIndex] != nullptr && neighbours[dirIndex]->gem->gemColour == gem->gemColour) {
		neighbours[dirIndex]->updatePartOfMatchSize(dirIndex, size);
	}
}