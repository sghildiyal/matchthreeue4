// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchThreePlayerController.h"

void AMatchThreePlayerController::handleMouseSelect(bool hitOccured, ACell* hitCell) {

	//if (hitOccured && hitCell != nullptr) {
	if (hitOccured && IsValid(hitCell)) {
		
		if (IsValid(currentSelectedCell)) {
			if (currentSelectedCell == hitCell) {
				currentSelectedCell->endMouseSelect();
				currentSelectedCell = nullptr;
			}
			else if (currentSelectedCell->isNeighbourOf(hitCell)) {
				if (IsValid(gameGrid)) {
					UE_LOG(LogTemp, Warning, TEXT("currentSelectedCell loc    %f, %f, %f"), currentSelectedCell->GetActorLocation().X, currentSelectedCell->GetActorLocation().Y, currentSelectedCell->GetActorLocation().Z);
					UE_LOG(LogTemp, Warning, TEXT("hitCell loc                %f, %f, %f"), hitCell->GetActorLocation().X, hitCell->GetActorLocation().Y, hitCell->GetActorLocation().Z);
					gameGrid->doGemSwap(currentSelectedCell, hitCell);
				}
				currentSelectedCell->endMouseSelect();
				currentSelectedCell = nullptr;
			}
			else {
				currentSelectedCell->endMouseSelect();
				hitCell->startMouseSelect();
				currentSelectedCell = hitCell;
			}
		}
		else {
			hitCell->startMouseSelect();
			currentSelectedCell = hitCell;
		}
	}
	else if (IsValid(currentSelectedCell)) {
		currentSelectedCell->endMouseSelect();
		currentSelectedCell = nullptr;
	}
}

void AMatchThreePlayerController::doGemSwap(ACell* cell1, ACell* cell2) {

}